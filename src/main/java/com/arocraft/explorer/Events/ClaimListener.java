package com.arocraft.explorer.Events;

import com.arocraft.explorer.Library.ClaimManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;

import java.util.UUID;

public class ClaimListener implements Listener {

    @EventHandler
    public void onBlockDamage(BlockDamageEvent event) {
        Location loc = event.getBlock().getLocation();
        UUID playerId = event.getPlayer().getUniqueId();

        // If any claims exist here, not just the event owner's claim.
        if (ClaimManager.isClaimed(loc)) {

            // ownerId is null if this location is not "claimed."
            UUID ownerId = ClaimManager.getOwnerIdFromLocation(loc);

            boolean isTrustedUser = ClaimManager.getClaim(ownerId).contains(playerId);

            // If the event owner does not own this claim and is not a trusted user.
            if ( !(playerId.equals(ownerId)) && !isTrustedUser ) {

                String ownerName = getOwnersName(ownerId);
                if ( ownerName != null ) {
                    event.getPlayer().sendMessage("This block is protected by " + ownerName + "'s claim.");
                } else {
                    event.getPlayer().sendMessage("This block is protected by someone's claim.");
                }

                // Cancel the damage block event (this one line does all the work).
                event.setCancelled(true);
            }

        }
    }

    private String getOwnersName(UUID ownerId) {
        String ownerName = null;
        try {
            ownerName = Bukkit.getServer().getPlayer(ownerId).getName();
        } catch (NullPointerException e) {
//            Bukkit.getLogger().log(Level.WARNING, "The owner of this claim is not online.");
        }
        if ( Bukkit.getServer().getOfflinePlayer(ownerId).getName() != null ) {
            ownerName = Bukkit.getServer().getOfflinePlayer(ownerId).getName();
        }
        return ownerName;
    }
}
