package com.arocraft.explorer.Library;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

// @link: https://www.spigotmc.org/wiki/config-files/

/**
 * All config.yml editing goes through this service.
 */
public class DataLayer {

    private static int adminIndex = 1;

    public static void writeClaimToConfig(FileConfiguration config, Claim claim) {
        String uuid = claim.getOwnerId().toString();

        config.set("claimdata." + uuid + ".x", claim.home.getX());
        config.set("claimdata." + uuid + ".y", claim.home.getY());
        config.set("claimdata." + uuid + ".z", claim.home.getZ());

        config.set("claimdata." + uuid + ".size", claim.getClaimSize());

        HashSet<UUID> guestList = claim.getTrustedGuestList();
        config.set("claimdata." + uuid + ".trustedguest", guestList.toString());
    }

    public static void writeAdminToConfig(FileConfiguration config, String adminUUId) {
        config.set("admins." + adminIndex++, adminUUId);
    }

    public static void removeClaimFromConfig(FileConfiguration config, UUID playerID) {
        config.set("claimdata." + playerID.toString(), null);
    }

    public static void loadDataFromConfig(JavaPlugin plugin, HashMap<UUID, Claim> claims, HashSet<UUID> admins) {
        FileConfiguration config = plugin.getConfig();

        if (config.getConfigurationSection("claimdata") != null) {

            // Cache the default World from the server.
            World world = plugin.getServer().getWorlds().get(0);

            // For each player having a Minecraft UUID value saved in the config.yml file.
            for (String uuid : config.getConfigurationSection("claimdata").getKeys(false)) {

                // Read the (x, y, z) coords from the config file.
                double x = config.getDouble("claimdata." + uuid + ".x");
                double y = config.getDouble("claimdata." + uuid + ".y");
                double z = config.getDouble("claimdata." + uuid + ".z");

                int size = config.getInt("claimdata." + uuid + ".size");

                // Create a new Claim from saved coords and current default World.
                Claim claim = new Claim(UUID.fromString(uuid) , new Location(world, x, y, z), size);

                // Get the trusted guest list from the config.
                String guestList = config.getString("claimdata." + uuid + ".trustedguest");

                // Convert the String representation of the guest list to actual UUID's
                guestList = guestList.replace("[", "").replace("]", "");
                guestList = guestList.trim();
                if (!guestList.isEmpty()) {
                    for (String str : guestList.split(", ")) {
                        // Then add them to the claim's trustedGuestList.
                        claim.addTrustedGuest(UUID.fromString(str));
                    }
                }

                // Load the stored claim data into the memory cache.
                claims.put(claim.getOwnerId(), claim);
            }
        }

        if (config.getConfigurationSection("admins") != null) {

            // Build Claim admins set.
            for (String i : config.getConfigurationSection("admins").getKeys(false)) {
                String adminId = config.getString("admins." + i);
                admins.add(UUID.fromString(adminId));
            }
        }
    }
}
