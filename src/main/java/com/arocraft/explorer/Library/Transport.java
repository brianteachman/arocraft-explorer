package com.arocraft.explorer.Library;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.HashSet;
import java.util.Random;

public class Transport {

    // using Medium size map (works on all consoles)
    // https://minecraft.gamepedia.com/World_size
    private static int MAX_MAPSIZE = 3000;

    private static Random randGen = new Random();

    public static HashSet<Material> notsafe_blocks = new HashSet<>();

    static{
        notsafe_blocks.add(Material.WATER);
        notsafe_blocks.add(Material.LAVA);
    }

    public static Location toTheWild(World world) {
//        double worldSize = world.getWorldBorder().getSize();
//        System.out.println("World size: " + worldSize);

        // Generate random location and find respective surface block elevation.
        int x = randGen.nextInt(MAX_MAPSIZE);
        int z = randGen.nextInt(MAX_MAPSIZE);
        Location randomLocation = new Location(world, x, 0, z);
        randomLocation.setY((double) world.getHighestBlockYAt(randomLocation) + 1);

        return randomLocation;
    }

    public static boolean isSafeLanding(Location target) {
        int x = target.getBlockX();
        int y = target.getBlockY();
        int z = target.getBlockZ();

        // Get the block to spawn at, the block under it and the block above it.
        Block block = target.getWorld().getBlockAt(x, y, z);
        Block below = target.getWorld().getBlockAt(x, y - 1, z);
        Block above = target.getWorld().getBlockAt(x, y + 1, z);

        // Make sure the block we are standing on is safe to land on and the 2 blocks above it are not solid.
        return !(notsafe_blocks.contains(below.getType()) || (block.getType().isSolid()) || (above.getType().isSolid()));
    }
}
