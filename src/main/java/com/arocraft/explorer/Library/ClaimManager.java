package com.arocraft.explorer.Library;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

public class ClaimManager {

    public static final int DEFAULT_CLAIM_SIZE = 20;

    // A Map of user ID's mapping to Sets of protected block ID's.
    private static HashMap<UUID, Claim> claims = new HashMap<>();

    // Contains players server-wide Claim admin privileges.
    private static HashSet<UUID> admins = new HashSet<>();

    // ------------------------------------------------------------------------
    // Server-wide, player based, plugin management
    // ------------------------------------------------------------------------

    public static void addAdmin(Player player) {
        admins.add(player.getUniqueId());
    }

    public static void removeAdmin(Player player) {
        admins.remove(player.getUniqueId());
    }

    public static boolean isAdmin(Player player) {
        return admins.contains(player.getUniqueId());
    }

    // ------------------------------------------------------------------------
    // Basic land and claim checking
    // ------------------------------------------------------------------------

    /**
     * Checks if Block (b) is protected by a player's "Claim."
     *
     * Complexity: O(2n)
     */
    public static boolean isBlockEncroaching(Block b) {
        for (Map.Entry<UUID, Claim> entry : claims.entrySet()) {
            if (entry.getValue().contains(b)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isClaimed(Location loc) {
        return isBlockEncroaching(loc.getBlock());
    }

    /**
     * Check if any part of a "Claim" is encroaching another player's "Claim."
     *
     * Complexity: O(2n^4)
     */
    private static boolean isClaimable(Location loc, int size) {
        int delta = (int) (size / 2);
        for(int x = loc.getBlockX() - delta; x <= loc.getBlockX() + delta; x++) {
            for(int y = loc.getBlockY() - delta; y <= loc.getBlockY() + delta; y++) {
                for(int z = loc.getBlockZ() - delta; z < loc.getBlockZ() + delta; z++) {

                    Block b = loc.getWorld().getBlockAt(x, y, z);
                    if ( isBlockEncroaching(b) ) { // 0(2n)
                        return false; // We can not claim this spot.
                    }
                }
            }
        }
        return true;
    }

    // ------------------------------------------------------------------------
    // Claim management
    // ------------------------------------------------------------------------

    /**
     * Set/Reset a players claim.
     */
    public static boolean setClaim(Player player, JavaPlugin plugin) {

        // Search all players Claims for overlap.
        if ( isClaimable(player.getLocation(), DEFAULT_CLAIM_SIZE) ) {

            // Add a new Claim referenced by the player's Minecraft ID to the working structure.
            claims.put(player.getUniqueId(), new Claim(player.getUniqueId(), player.getLocation(), DEFAULT_CLAIM_SIZE));

            // Save the players home location to the config.yml file.
            savePlayerClaim(plugin, player);

            return true;
        }

        return false;
    }

    public static void unsetClaim(Player player, JavaPlugin plugin) {

        // Removed the Claim referenced by the player's Minecraft ID.
        claims.remove(player.getUniqueId());

        // Save the players home location to the config.yml file.
        removePlayerClaim(player, plugin);
    }

    public static void setClaimSize(Player player, int size) {
        claims.get(player.getUniqueId()).setClaimSize(size);
    }

    /**
     * Save current claim data to config.yml.
     */
    public static void updateClaim(Player player, JavaPlugin plugin) {
        savePlayerClaim(plugin, player);
    }

    public static Claim getClaim(Player player) {
        return claims.get(player.getUniqueId());
    }

    public static Claim getClaim(UUID playerId) {
        return claims.get(playerId);
    }

    public static UUID getOwnerIdFromLocation(Location loc) {
        UUID owner = null;
        for (Map.Entry<UUID, Claim> claim : claims.entrySet()) {

            if (claim.getValue().contains(loc.getBlock())) {
                owner = claim.getKey();
            }
        }
        return owner;
    }

    public static Location getHomeLocation(Player player) {
        if (claims.get(player.getUniqueId()) != null) {
            return claims.get(player.getUniqueId()).home;
        } else {
            return Bukkit.getServer().getWorld("world").getSpawnLocation();
        }
    }

    // ------------------------------------------------------------------------
    // Trusted guest management
    // ------------------------------------------------------------------------

    /**
     * Add a "guest" Player to some other Player's claim's trusted guest list.
     *
     * Returns true if guest was successfully added to the player's guest list.
     */
    public static boolean addGuest(Player player, Player guest) {
        Claim claim = claims.get(player.getUniqueId());
        if (claim != null) {
            claim.addTrustedGuest(guest.getUniqueId());
            return true;
        }
        return false;
    }

    public static boolean removeGuest(Player player, Player guest) {
        Claim claim = claims.get(player.getUniqueId());
        if (claim != null) {
            claim.removeTrustedGuest(guest.getUniqueId());
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------------
    // Data persistence (config.yml)
    // ------------------------------------------------------------------------

    /**
     * Reads config.yml to determine if the plugin is "enabled" (enabled ?= true).
     */
    public static boolean isPluginEnabled(JavaPlugin plugin) {
        return plugin.getConfig().getBoolean("enabled");
    }

    /**
     * Save the players current location to the config.yml file.
     */
    private static void savePlayerClaim(JavaPlugin plugin, Player player) {
        DataLayer.writeClaimToConfig(plugin.getConfig(), claims.get(player.getUniqueId()));
        plugin.saveConfig();
    }

    public static void removePlayerClaim(Player player, JavaPlugin plugin) {
        DataLayer.removeClaimFromConfig(plugin.getConfig(), player.getUniqueId());
        plugin.saveConfig();
    }

    public static void saveConfigData(JavaPlugin plugin) {

        // Save all players Claim data to config.yml.
        for (Map.Entry<UUID, Claim> claim : claims.entrySet()) {
            DataLayer.writeClaimToConfig(plugin.getConfig(), claims.get(claim.getKey()));
        }

        //Save all Claim admins to config.yml.
        for (UUID admin : admins) {
            DataLayer.writeAdminToConfig(plugin.getConfig(), admin.toString());
        }

        // Write file and save once done.
        plugin.saveConfig();
    }

    /**
     * Returns a Location of a players claim from the config.yml file.
     */
    public static void loadConfigData(JavaPlugin plugin) {
        //
        DataLayer.loadDataFromConfig(plugin, claims, admins);
    }

}
