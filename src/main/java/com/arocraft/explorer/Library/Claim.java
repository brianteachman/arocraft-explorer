package com.arocraft.explorer.Library;

import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.HashSet;
import java.util.UUID;

public class Claim {

    private UUID ownerId;

    private int sideLength;

    private final HashSet<UUID> trustedGuestList;

    private HashSet<Block> protectedBlocksList;

    public Location home;

    public Claim(UUID playerId, Location loc, int size) {
        ownerId = playerId;
        home = loc;
        sideLength = size;
        trustedGuestList = new HashSet<>();
        buildProtectedBlockList();
    }

    public String toString() {
        return "{ x=" + home.getBlockX()
                + ", y=" + home.getBlockY()
                + ", z=" + home.getBlockZ()
                + ", trustedguest=" + trustedGuestList.toString() + " }";
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void addTrustedGuest(UUID playerId) {
        this.trustedGuestList.add(playerId);
    }

    public void removeTrustedGuest(UUID uniqueId) {
        this.trustedGuestList.remove(uniqueId);
    }

    public HashSet<UUID> getTrustedGuestList() {
        return trustedGuestList;
    }

    public boolean contains(Block block) {
        return protectedBlocksList.contains(block);
    }

    public boolean contains(UUID payerId) {
        return trustedGuestList.contains(payerId);
    }

    public int getClaimSize() {
        return sideLength;
    }

    /**
     * Update claim size (sideLength) and rebuild protected blocks list.
     *
     * Note: that the Claim does not care if two claims overlap,
     *       it leaves that decision up to the ClaimManager.
     *
     * @param sideLength
     */
    public void setClaimSize(int sideLength) {
        this.sideLength = sideLength;

        // We updated the claim size so rebuild protected blocks set.
        buildProtectedBlockList();
    }

    /**
     * Build the set of protected blocks.
     *
     * Note: that the Claim does not care if two claims overlap,
     *       it leaves that decision up to the ClaimManager.
     */
    private void buildProtectedBlockList() {
        this.protectedBlocksList = new HashSet<>();

        int delta = (int) (sideLength / 2);
        for(int x = home.getBlockX() - delta; x <= home.getBlockX() + delta; x++) {
            for(int y = home.getBlockY() - delta; y <= home.getBlockY() + delta; y++) {
                for(int z = home.getBlockZ() - delta; z < home.getBlockZ() + delta; z++) {

                    Block b = home.getWorld().getBlockAt(x, y, z);
                    this.protectedBlocksList.add(b);
                }
            }
        }
    }

}
