package com.arocraft.explorer;

import com.arocraft.explorer.Commands.ClaimExecutor;
import com.arocraft.explorer.Commands.HomeExecutor;
import com.arocraft.explorer.Commands.WildExecutor;

import com.arocraft.explorer.Events.ClaimListener;
import com.arocraft.explorer.Library.ClaimManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Explorer extends JavaPlugin {

    @Override
    public void onEnable() {

        // Load in saved config options.
        getConfig().options().copyDefaults();
        // Save the raw contents of the default config.yml file to the location retrievable by getConfig().
        saveDefaultConfig();

        // Enable our plugin commands.
        getCommand("claim").setExecutor(new ClaimExecutor(this));
        getCommand("home").setExecutor(new HomeExecutor());
        getCommand("wild").setExecutor(new WildExecutor());

        // Register the event that checks if block belongs to a claim when damaged.
        getServer().getPluginManager().registerEvents(new ClaimListener(), this);

        // Load the claims data (from config.yml) into the ClaimManager cache.
        ClaimManager.loadConfigData(this);
    }

    @Override
    public void onDisable() {
        // Persist player data; save the ClaimManager cache (HashMap) to the config.yml file.
        ClaimManager.saveConfigData(this);
    }
}
