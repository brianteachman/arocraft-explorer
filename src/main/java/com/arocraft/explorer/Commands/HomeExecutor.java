package com.arocraft.explorer.Commands;

import com.arocraft.explorer.Library.ClaimManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HomeExecutor implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("home")) {

            // Make sure the sender is a player.
            if (!(sender instanceof Player)) {
                sender.sendMessage("Only players can teleport home.");
                return true;
            }

            Player player = (Player) sender;
            player.teleport(ClaimManager.getHomeLocation(player));

            return true;
        }

        return false;
    }
}
