package com.arocraft.explorer.Commands;

import com.arocraft.explorer.Library.Transport;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.World;

// @link https://hub.spigotmc.org/javadocs/spigot/org/bukkit/package-summary.html

public class WildExecutor implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("wild")) {

            // Make sure the sender is a player.
            if (!(sender instanceof Player)) {
                sender.sendMessage("Only players can be teleported.");
                return true;
            }

            //If a player issued this command, send them somewhere random.
            Player player = (Player) sender;
            Location randomSpot = transportToWild();
            player.teleport(randomSpot);

            // Let server/clients know it happened.
            String message = player.getName() + " has teleported into the wild. <" + randomSpot.getX() + ", " + randomSpot.getZ() + ">";
            Bukkit.broadcastMessage(message);

            return true;
        }
        return false;
    }

    /**
     * @return Location Some random location on the map.
     */
    private Location transportToWild() {

        // Cache the world to work with.
        World world = Bukkit.getWorld("world");

        // Transport player to some random location.
        Location randomSpot = Transport.toTheWild(world);

        // If the randomly selected location is not a safe landing, pick a new location.
        while ( ! Transport.isSafeLanding(randomSpot)) {
            randomSpot = Transport.toTheWild(world);
        }

        return randomSpot;
    }

}
