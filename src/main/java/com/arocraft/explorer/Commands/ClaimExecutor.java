package com.arocraft.explorer.Commands;

import com.arocraft.explorer.Explorer;
import com.arocraft.explorer.Library.Claim;
import com.arocraft.explorer.Library.ClaimManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

// @link https://hub.spigotmc.org/javadocs/spigot/org/bukkit/package-summary.html
// @link https://papermc.io/javadocs/paper/1.16/overview-summary.html

public class ClaimExecutor implements CommandExecutor {

    // Used to access the config options.
    Explorer plugin;

    public ClaimExecutor(Explorer plugin) {
        // Cache the plugin class to fetch the config options.
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!ClaimManager.isPluginEnabled(plugin)) {
            sender.sendMessage("You need to enable the Explorer plugin for '/claim' to work.");
            return true;
        }

        // This are cached just for readability.
        boolean claimCommand = cmd.getName().equalsIgnoreCase("claim");

        // If a server operator issued this command.
        if (!(sender instanceof Player)) {
        // Only server admins (operators) can issue these commands.

            if (claimCommand && (args.length == 3)) {

                // ------------------------------------------------------------
                // /claim admin add|remove <playername>
                // ------------------------------------------------------------
                if (args[0].equalsIgnoreCase("admin")) {

                    // Try to create a Player object from <playername>.
                    Player newAdmin = Bukkit.getServer().getPlayer(args[2]);

                    // Is the player online (can create a new Player object).
                    if (newAdmin != null) {

                        if ( args[1].equalsIgnoreCase("add") ) {
                            ClaimManager.addAdmin(newAdmin);
                            newAdmin.sendMessage("You have been given Claim Admin privileges.");
                            sender.sendMessage(newAdmin.getName() + " has been given Claim Admin privileges.");
                        }
                        else if ( args[1].equalsIgnoreCase("remove") ) {
                            ClaimManager.removeAdmin(newAdmin);
                            newAdmin.sendMessage("You have been removed from Claim Admin privileges.");
                            sender.sendMessage(newAdmin.getName() + " has been removed from Claim Admin privileges.");
                        }

                    } else { // Player's not online, so ...
                        offlinePlayerMessage(sender, args[1]);
                    }
                    return true;
                }

                // ------------------------------------------------------------
                // /claim resize <playername> <size>
                // ------------------------------------------------------------
                else if (args[0].equalsIgnoreCase("resize")) {
//                    ClaimManager.setClaimSize(player, size);
                    sender.sendMessage("You said resize?");
                }

            } else if (claimCommand && (args.length == 2)) {

                // ------------------------------------------------------------
                // /claim reclaim <playername>
                // ------------------------------------------------------------
                if (args[0].equalsIgnoreCase("reclaim")) {
//                    ClaimManager.unsetClaim(player, plugin);
                    sender.sendMessage("You said reclaim?");
                }

            }

            return true;

        } else { // An online Player issued these commands:

            // We'll need the player
            Player player = (Player) sender;

            // "/claim" command was called followed by two arguments.
            if (claimCommand && (args.length == 2)) {

                // ------------------------------------------------------------
                // /claim add <playername>
                // ------------------------------------------------------------
                if (args[0].equalsIgnoreCase("add")) {

                    // "/claim add <guestname>" called: Get a Player object for our guest.
                    Player trustedGuest = Bukkit.getServer().getPlayer(args[1]);

                    // If the player called the "add" command followed by the guest name
                    // and a guest object actually initializes to someone on the server.
                    if (trustedGuest != null) {

                        // Then add that guest to the player's trustedGuestList.
                        if ( ClaimManager.addGuest(player, trustedGuest) ) {
                            player.sendMessage(trustedGuest.getName() + " can now edit your claim.");
                        } else {
                            player.sendMessage("You do not currently have a claim set.\nYou must first set it by using the /claim command.");
                        }
                    } else {
                        offlinePlayerMessage(player, args[1]);
                    }
                    return true;
                }

                // ------------------------------------------------------------
                // /claim remove <playername>
                // ------------------------------------------------------------
                else if (args[0].equalsIgnoreCase("remove")) {

                    // "/claim add <guestname>" called: Get a Player object for our guest.
                    Player trustedGuest = Bukkit.getServer().getPlayer(args[1]);

                    // If the player called the "add" command followed by the guest name
                    // and a guest object actually initializes to someone on the server.
                    if (trustedGuest != null) {

                        // Then add that guest to the player's trustedGuestList.
                        if ( ClaimManager.removeGuest(player, trustedGuest) ) {
                            player.sendMessage(trustedGuest.getName() + " can no longer edit your claim.");
                        } else {
                            player.sendMessage("You do not currently have a claim set.\nYou must first set it by using the /claim command.");
                        }
                    } else {
                        offlinePlayerMessage(player, args[1]);
                    }
                    return true;
                }

                // ------------------------------------------------------------
                // /claim resize <sidelength>
                // ------------------------------------------------------------
                else if (args[0].equalsIgnoreCase("resize")) {

                    if (ClaimManager.isAdmin(player)) {
                        int size = Integer.parseInt(args[1]);
                        ClaimManager.setClaimSize(player, size);
                        player.sendMessage(player.getName() + " updated their claim size to an area of " + Math.pow(size, 3) + " blocks^3. (Yes, that's " + size + "^3 blocks)");
                    } else {
                        player.sendMessage("I'm afraid I cant let you do that, Hal.");
                    }
                    return true;
                }

                return true;

            // "/claim" command was called followed by a single argument.
            } else if (claimCommand && (args.length == 1)) {

                // ------------------------------------------------------------
                // /claim update
                // ------------------------------------------------------------
                if (args[0].equalsIgnoreCase("update")) {

                    // Save the current claim data to config.yml.
                    ClaimManager.updateClaim(player, plugin);
                    player.sendMessage(player.getName() + " updated their claim data.");

                    return true;
                }

                // ------------------------------------------------------------
                // /claim set
                // ------------------------------------------------------------
                else if (args[0].equalsIgnoreCase("set")) {

                    if (ClaimManager.getClaim(player) != null) {
                        player.sendMessage("You already have a claim set. Call '/claim unset' first to be able to set a new claim.");

                    } else if ( ClaimManager.setClaim(player, plugin) ) {
                        String name = player.getName();
                        String x = String.valueOf(player.getLocation().getBlockX());
                        String z = String.valueOf(player.getLocation().getBlockZ());
                        player.sendMessage(name + " has set <" + x + ", " + z + "> as their home claim.");

                    } else {
                        player.sendMessage("This spot is to close to someones claim. Try somewhere else.");
                    }

                    return true;
                }

                // ------------------------------------------------------------
                // /claim unset
                // ------------------------------------------------------------
                else if (args[0].equalsIgnoreCase("unset")) {

                    ClaimManager.unsetClaim(player, plugin);
                    player.sendMessage(player.getName() + " has just unset their claim.");

                    return true;
                }

            // ------------------------------------------------------------
            // /claim
            // ------------------------------------------------------------
            } else if (claimCommand) {

                String name = player.getName();

                Claim claim = ClaimManager.getClaim(player);
                if (claim != null) {

                    String x = String.valueOf(player.getLocation().getBlockX());
                    String z = String.valueOf(player.getLocation().getBlockZ());
                    player.sendMessage(name + " owns <" + x + ", " + z + "> as their home claim.");

                } else {
                    player.sendMessage(name + " does not own a 'claim'. Use the command '/claim set'.");
                }

                return true;
            }
        }
        return false;
    }

    // Check is the requested player has ever logged in to the server.
    private void offlinePlayerMessage(CommandSender sender, String name) {
        for (OfflinePlayer p : Bukkit.getOfflinePlayers()) {
            if (p.getName().equalsIgnoreCase(name)) {
                if (p.hasPlayedBefore()) {
                    sender.sendMessage("Player name " + name + " is not online right now, try again later.");
//                    Player foundPlayer = p.getPlayer(); // Only works if player is online.
//                    sender.sendMessage(foundPlayer.getName() + " found." + "(" + foundPlayer.getUniqueId() + ")");
                } else {
                    sender.sendMessage(name + " has never played on this server, try again when " + name + " is online.");
                }
                break; // We found a match, return early.
            } else {
                sender.sendMessage("Server can't find the player named " + name + ".");
            }
        }
    }

}
