# Spigot Plugin: AroCraft Explorer

This Minecraft spigot plugin was developed by Brian Teachman `(0logn)`, for [AroCraft Server](https://trello.com/b/gFP94614/arocraft-server) Project `(rooman08)`.

## Command List
Player Commands: 

    /claim
    /claim set
    /claim unset
    /claim add <player>
    /claim remove <player>
    /claim resize <size>
    /home
    /wild

Server Commands: 

    /claim admin add <player>
    /claim admin remove <player>
    /claim resize <player> <size>
    /claim reclaim <player>

---

## Client side (player) commands

### /claim
Display's your claim's home location if you have one set.

### /claim set
Set players current location as "home" and protect a 20 block radius. 
Other players cannot stake a "claim" within 20 blocks of your claim (home).

### /claim set
Unsets players current "home" location and resets it to the current spawn.
@TODO: Clear the trusted guest list?

### /claim add [playername]
Add [playername] to your trusted guest list. [playername] can edit your claim.

### /claim remove [playername]
Removes [playername] from your trusted guest list. [playername] can no longer edit your claim.

### /claim resize [size]
If the Player that is calling '/claim resize' has been added to the ClaimManager's admin list, then that player can resize their own claim to the [size] passed as the 2nd argument.

### /home
Teleport player to the players claim (home) or the last spawn location.

### /wild
Teleport player somewhere into the wild and provide a safe landing.

---

## Server side commands

### /claim admin add [playername]
Add [playername] to the ClaimManager's admin list.

### /claim admin remove [playername]
Remove [playername] to the ClaimManager's admin list.

### /claim resize [playername] [size]
If [playername] has a claim staked, then resize it to the [size] given.

### /claim reclaim [playername]
Remove [playername]'s claim from the ClaimManager.
